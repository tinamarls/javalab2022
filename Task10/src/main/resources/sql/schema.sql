drop table if exists product;

create table product(
    id bigserial primary key,
    name_of_product varchar(50),
    color varchar(20),
    amount integer,
    price integer,
    producer varchar(30)
);

