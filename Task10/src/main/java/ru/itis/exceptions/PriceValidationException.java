package ru.itis.exceptions;

public class PriceValidationException extends RuntimeException{
    public PriceValidationException(String message) {
        super(message);
    }
}
