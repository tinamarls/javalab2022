package ru.itis.ui;

import ru.itis.services.ProductService;

import java.util.Scanner;

public class UI {

    private final Scanner scanner = new Scanner(System.in);

    private final ProductService productService;

    public UI(ProductService productService) {
        this.productService = productService;
    }

    public void start() {
        while (true) {
            printMainMenu();

            String command = scanner.nextLine();
            System.out.println(command);

            switch (command) {
                case "1" -> {
                    String nameOfProduct = scanner.nextLine();
                    String color = scanner.nextLine();
                    String amount = scanner.nextLine();
                    String producer = scanner.nextLine();
                    String price = scanner.nextLine();

                    if (productService.insertProduct(nameOfProduct, color, Integer.parseInt(amount), producer, Integer.parseInt(price))) {
                        System.out.println("Товар успешно добавлен");
                    } else {
                        System.out.println("Проблемы с добавлением товара");
                    }
                }
                case "2" -> System.out.println(productService.getAllProducts());
                case "3" -> {
                    String id = scanner.nextLine();
                    System.out.println(id);
                    productService.deleteProductByID(Long.valueOf(id));
                }
                default -> System.out.println("Команда не найдена");
            }
        }
    }

    private void printMainMenu(){
        System.out.println("1. Добавить товар");
        System.out.println("2. Посмотреть список товаров");
        System.out.println("3. Удалить товар по id");
    };
}
