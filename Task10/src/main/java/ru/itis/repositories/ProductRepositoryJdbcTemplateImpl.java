package ru.itis.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.itis.models.Product;

import javax.sql.DataSource;
import java.util.*;

public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from product where id = :id;";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update product set " +
            "name_of_product = :name_of_product, color = :color," +
            " amount = :amount, price= :price, producer= :producer where id = :id;";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from product where id = :id";

    //language=SQL
    private static final String SQL_SELECT_BY_PRICE_CHEAPER_THAN_ORDER_BY_AMOUNT_DESC =
            "select * from product where price < :price order by amount desc";


    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productMapper = ((rs, rowNum) ->
            Product.builder()
                    .id(rs.getLong("id"))
                    .nameOfProduct(rs.getString("name_of_product"))
                    .color(rs.getString("color"))
                    .amount(rs.getInt("amount"))
                    .producer(rs.getString("producer"))
                    .price(rs.getInt("price"))
                    .build());

    @Override
    public List<Product> findAll() {

        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    productMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }

    }

    @Override
    public void delete(Long id) {
        namedParameterJdbcTemplate.update(SQL_DELETE_BY_ID, Collections.singletonMap("id", id));
    }

    @Override
    public void save(Product product) {
        Map<String, Object> params = new HashMap<>();
        params.put("name_of_product", product.getNameOfProduct());
        params.put("color", product.getColor());
        params.put("amount", product.getAmount());
        params.put("price", product.getPrice());
        params.put("producer", product.getProducer());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("product")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(params)).longValue();

        product.setId(id);
    }

    @Override
    public void update(Product product) {
        Map<String, Object> params = new HashMap<>();
        params.put("name_of_product", product.getNameOfProduct());
        params.put("color", product.getColor());
        params.put("amount", product.getAmount());
        params.put("price", product.getPrice());
        params.put("producer", product.getProducer());
        params.put("id", product.getId());
        namedParameterJdbcTemplate.update(SQL_UPDATE_BY_ID, params);
    }

    @Override
    public List<Product> findAllByPriceCheaperThanOrderByAmountDesc(Integer minPrice) {
        return namedParameterJdbcTemplate.query(SQL_SELECT_BY_PRICE_CHEAPER_THAN_ORDER_BY_AMOUNT_DESC,
                Collections.singletonMap("price", minPrice),productMapper);
    }
}

