package ru.itis.repositories;

import ru.itis.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {
    List<Product> findAll();

    Optional<Product> findById(Long id);

    void delete(Long id);

    void save(Product product);

    void update(Product product);

    List<Product> findAllByPriceCheaperThanOrderByAmountDesc(Integer minPrice);
}
