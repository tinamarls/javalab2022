package ru.itis.services;

import ru.itis.models.Product;

import java.util.List;

public interface ProductService {

    boolean insertProduct(String nameOfProduct, String color, int amount, String producer, int price);

    List<Product> getAllProducts();

    void deleteProductByID(Long id);
}
