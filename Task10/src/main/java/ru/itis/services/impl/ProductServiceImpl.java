package ru.itis.services.impl;

import ru.itis.exceptions.PriceValidationException;
import ru.itis.models.Product;
import ru.itis.repositories.ProductRepository;
import ru.itis.services.ProductService;
import ru.itis.validation.PriceValidator;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    private final PriceValidator priceValidator;

    private final ProductRepository productRepository;

    public ProductServiceImpl(PriceValidator priceValidator, ProductRepository productRepository) {
        this.priceValidator = priceValidator;
        this.productRepository = productRepository;
    }

    public boolean insertProduct(String nameOfProduct, String color, int amount, String producer, int price){
        try{
            this.priceValidator.validate(price);
            Product product = Product.builder()
                    .nameOfProduct(nameOfProduct)
                    .color(color)
                    .amount(amount)
                    .producer(producer)
                    .price(price)
                    .build();
            productRepository.save(product);

            return true;
        } catch (PriceValidationException e){
            System.err.println(e.getMessage());
            return false;
        }
    }

    public List<Product> getAllProducts(){
        return(productRepository.findAll());
    };

    public void deleteProductByID(Long id){
        productRepository.delete(id);
    };
}
