package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.models.Product;
import ru.itis.repositories.ProductRepository;
import ru.itis.repositories.ProductRepositoryJdbcTemplateImpl;
import ru.itis.ui.UI;

import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        UI ui = context.getBean(UI.class);

        ui.start();

//        Properties properties = new Properties();
//        try {
//            properties.load(Main.class.getResourceAsStream("/db.properties"));
//        } catch (IOException ex) {
//            throw new IllegalArgumentException(ex);
//        }
//        HikariConfig config = new HikariConfig();
//        config.setJdbcUrl(properties.getProperty("db.url"));
//        config.setUsername(properties.getProperty("db.user"));
//        config.setPassword(properties.getProperty("db.password"));
//        config.setMaximumPoolSize(20);
//
//        HikariDataSource dataSource = new HikariDataSource(config);
//
//        ProductRepository productRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);
//
//        System.out.println(productRepository.findAll());
//        System.out.println(productRepository.findById(6L));
//        productRepository.update(new Product(6L,"Джинсы", "Синий", 1000, "Zara", 5000));
//        productRepository.delete(2L);
//        System.out.println(productRepository.findAllByPriceCheaperThanOrderByAmountDesc(2600));

    }
}