package ru.itis.validation;

import ru.itis.exceptions.PriceValidationException;

public interface PriceValidator {

    void validate(int price) throws PriceValidationException;
}
