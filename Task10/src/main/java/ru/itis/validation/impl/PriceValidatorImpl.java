package ru.itis.validation.impl;

import ru.itis.exceptions.PriceValidationException;
import ru.itis.validation.PriceValidator;

public class PriceValidatorImpl implements PriceValidator {
//
//    public PriceValidatorImpl() {
//    }

    public void validate(int price) throws PriceValidationException{
        if(price <= 0) {
            throw new PriceValidationException("Цена продукта не может быть отрицательной");
        }
    };
}
