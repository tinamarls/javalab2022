package ru.itis.dto.user;

import jakarta.servlet.http.HttpServletRequest;

public class HttpFormsConverter {
    public static SignUpDto from(HttpServletRequest request) {
        return SignUpDto.builder()
                .firstName(request.getParameter("firstName"))
                .lastName(request.getParameter("lastName"))
                .email(request.getParameter("username"))
                .password(request.getParameter("password"))
                .build();
    }
}

