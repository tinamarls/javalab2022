package ru.itis.dto.product;

import jakarta.servlet.http.HttpServletRequest;
import ru.itis.dto.product.ProductDto;

public class HttpFormConverter {
    public static ProductDto from(HttpServletRequest req) {
        return ProductDto.builder()
                .name(req.getParameter("name"))
                .color(req.getParameter("color"))
                .amount(Integer.parseInt(req.getParameter("amount")))
                .producer(req.getParameter("producer"))
                .price(Integer.parseInt(req.getParameter("price")))
                .build();
    }

}

