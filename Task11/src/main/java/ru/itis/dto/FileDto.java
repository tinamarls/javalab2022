package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.models.FileInfo;

import java.io.InputStream;
import java.nio.file.Path;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FileDto {
    private Long size;
    private String fileName;
    private String mimeType;
    private String originalFileName;
    private InputStream fileStream;
    private String description;
    private Path path;

    public static FileDto from(FileInfo fileInfo) {
        return FileDto.builder()
                .size(fileInfo.getSize())
                .description(fileInfo.getDescription())
                .originalFileName(fileInfo.getOriginalFileName())
                .mimeType(fileInfo.getMimeType())
                .fileName(fileInfo.getStorageFileName())
                .build();
    }

}
