package ru.itis.constants;

public class AllPaths {

    public static final String APPLICATION_PREFIX = "/app";
    public static final String PRODUCTS_PATH = "/products";
    public static final String SAVE_PATH = "/save";
    public static final String FIND_PATH = "/find";
    public static final String SEARCH_PATH = "/search";
    public static final String LIVE_SEARCH_PATH = "/search/live";
    public static final String PROFILE_PATH = "/profile";
    public static final String SIGN_IN_PATH = "/signIn";
    public static final String SIGN_IN_PAGE = "/signIn.html";
    public static final String SIGN_UP_PAGE = "/signUp.html";
    public static final String SIGN_UP_PATH = "/signUp";
    public static final String UPLOAD_FILE_PATH = "/files/upload";
    public static final String FILES_PATH = "/files";

}
