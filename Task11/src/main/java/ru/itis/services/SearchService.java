package ru.itis.services;

import ru.itis.dto.product.ProductDto;

import java.util.List;

public interface SearchService {

    List<ProductDto> searchBy(String query);

}
