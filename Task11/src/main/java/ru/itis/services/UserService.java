package ru.itis.services;

import ru.itis.dto.user.SignUpDto;
import ru.itis.dto.user.UserDto;
import ru.itis.models.FileInfo;
import ru.itis.models.User;

import java.util.List;

public interface UserService {
    void signUp(SignUpDto signUpData);
    List<UserDto> getAllUsers();

    void addPhotoForUser(User user, FileInfo fileInfo);

}
