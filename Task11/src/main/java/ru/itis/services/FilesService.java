package ru.itis.services;

import ru.itis.dto.FileDto;
import ru.itis.models.FileInfo;

public interface FilesService {
    FileInfo upload(FileDto file);

    FileDto getFile(Long idFile);

}
