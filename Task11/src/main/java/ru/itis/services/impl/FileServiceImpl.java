package ru.itis.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.itis.dto.FileDto;
import ru.itis.models.FileInfo;
import ru.itis.repositories.FilesRepository;
import ru.itis.services.FilesService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static ru.itis.dto.FileDto.from;

@Service
public class FileServiceImpl implements FilesService {

    @Value("${storage.path}")
    private String storagePath;

    private final FilesRepository filesRepository;


    public FileServiceImpl(FilesRepository filesRepository) {
        this.filesRepository = filesRepository;
    }

    @Override
    public FileInfo upload(FileDto file) {
        String originalFileName = file.getFileName();
        String extension = originalFileName.substring(originalFileName.lastIndexOf("."));
        String storageFileName = UUID.randomUUID() + extension;

        FileInfo fileInfo = FileInfo.builder()
                .description(file.getDescription())
                .size(file.getSize())
                .mimeType(file.getMimeType())
                .originalFileName(originalFileName)
                .storageFileName(storageFileName)
                .build();

        try {
            Files.copy(file.getFileStream(), Paths.get(storagePath + storageFileName));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.println("до" + fileInfo);
        filesRepository.save(fileInfo);

        return fileInfo;
    }

    @Override
    public FileDto getFile(Long idFile) {
        FileInfo file = filesRepository.findByStorageFileName(idFile).orElseThrow();
        FileDto fileDto = from(file);
        fileDto.setPath(Paths.get(storagePath + "\\" + file.getStorageFileName()));
        return fileDto;
    }

}
