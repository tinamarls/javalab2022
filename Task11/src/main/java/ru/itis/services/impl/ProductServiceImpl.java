package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.product.ProductDto;
import ru.itis.models.Product;
import ru.itis.repositories.ProductRepository;
import ru.itis.services.ProductService;

import java.util.List;

@RequiredArgsConstructor
@Service

public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public void insertProduct(ProductDto productDto) {
        Product product = Product.builder()
                .nameOfProduct(productDto.getName())
                .color(productDto.getColor())
                .amount(productDto.getAmount())
                .producer(productDto.getProducer())
                .price(productDto.getPrice())
                .build();
        productRepository.save(product);

    }

    public List<Product> getAllProducts(){
        return(productRepository.findAll());
    }

    public List<Product> getAllBy(String id, String nameOfProduct, String color, String amount, String price, String producer){
        return (productRepository.findBy(id, nameOfProduct, color, amount, price, producer));
    }

    @Override
    public List<Product> getAllOrderBy(String columnName, String descOrAsc) {
        return productRepository.findAllProductSortedBy(columnName, descOrAsc);
    }

}
