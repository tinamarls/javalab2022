package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.product.ProductDto;
import ru.itis.repositories.ProductRepository;
import ru.itis.services.SearchService;

import java.util.Collections;
import java.util.List;

import static ru.itis.dto.product.ProductDto.fromList;

@RequiredArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {

    private final ProductRepository productRepository;
    @Override
    public List<ProductDto> searchBy(String query) {
        if(query == null || query.equals("")){
            return Collections.emptyList();
        }
        return fromList(productRepository.findAllByNameOrColorOrProducerLike(query));
    }
}
