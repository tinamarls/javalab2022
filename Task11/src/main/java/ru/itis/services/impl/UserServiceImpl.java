package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.user.SignUpDto;
import ru.itis.dto.user.UserDto;
import ru.itis.models.FileInfo;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;
import ru.itis.services.UserService;

import java.util.List;

import static ru.itis.dto.user.UserDto.from;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository usersRepository;

    @Override
    public void signUp(SignUpDto signUpData) {
        User user = User.builder()
                .firstName(signUpData.getFirstName())
                .lastName(signUpData.getLastName())
                .email(signUpData.getEmail())
                .password(signUpData.getPassword())
                .build();

        usersRepository.save(user);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return from(usersRepository.findAll());
    }

    @Override
    public void addPhotoForUser(User user, FileInfo fileInfo) {
        usersRepository.setFileId(user, fileInfo.getId());
        user.setFileId(fileInfo.getId());
    }


}
