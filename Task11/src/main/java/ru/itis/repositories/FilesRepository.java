package ru.itis.repositories;

import ru.itis.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FilesRepository {
    void save(FileInfo fileInfo);

    Optional<FileInfo> findByStorageFileName(Long id);

    List<String> findAllStorageNamesByType(String ... types);

}
