package ru.itis.repositories.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;

import javax.sql.DataSource;
import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_USERS = "select * from users;";

    //language=SQL
    private static final String SQL_SET_FILE_ID = "update users set id_file_info = :idFile where id = :id;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from users where id = :id;";

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "" +
            "select * from users where email = :email";

    private static final RowMapper<User> userMapper = (row, rowNumber) -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .age(row.getObject("age", Integer.class))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .fileId(row.getLong("id_file_info"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<User> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_USERS, userMapper);
    }

    @Override
    public void save(User user) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("first_name", user.getFirstName());
        paramsAsMap.put("last_name", user.getLastName());
        paramsAsMap.put("email", user.getEmail());
        paramsAsMap.put("password", user.getPassword());
        paramsAsMap.put("phone_number", "default");

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("users")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();


        user.setId(id);
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    userMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findOneByEmail(String email) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL,
                    Collections.singletonMap("email", email),
                    userMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void setFileId(User user, Long idFile) {
        Map<String, Object> params = new HashMap<>();
        params.put("idFile", idFile);
        params.put("id", user.getId());

        namedParameterJdbcTemplate.update(SQL_SET_FILE_ID, params);
    }

}
