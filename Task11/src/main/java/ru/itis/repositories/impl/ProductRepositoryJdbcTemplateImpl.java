package ru.itis.repositories.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.Product;
import ru.itis.repositories.ProductRepository;

import javax.sql.DataSource;
import java.util.*;

@Repository
public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product;";

    //language=SQL
    private static final String SQL_LIKE_BY_NAME_OR_COLOR_OR_PRODUCER = "select * from product where" +
            "(name_of_product ilike :query or color ilike :query or producer ilike :query);";

    //language=SQL
    private static final String SQL_SELECT_SORTED_BY = "select * from product order by ";


    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productMapper = ((rs, rowNum) ->
            Product.builder()
                    .id(rs.getLong("id"))
                    .nameOfProduct(rs.getString("name_of_product"))
                    .color(rs.getString("color"))
                    .amount(rs.getInt("amount"))
                    .producer(rs.getString("producer"))
                    .price(rs.getInt("price"))
                    .build());

    @Override
    public List<Product> findAll() {

        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public List<Product> findBy(String id, String nameOfProduct, String color, String amount, String price, String producer) {

        Map<String, Object> params = new HashMap<>();

        //language=SQL
        StringBuilder SQL_SELECT_BY_ID = new StringBuilder("select * from product where ");

        int counter = 0;

        if(!id.isEmpty()){
            SQL_SELECT_BY_ID.append("id:= id");
            params.put("id", Long.parseLong(id));
            counter += 1;
        }

        if (!nameOfProduct.isEmpty()){
            if (counter != 0){
                SQL_SELECT_BY_ID.append(" and ");
            }
            SQL_SELECT_BY_ID.append("name_of_product = :name_of_product");
            params.put("name_of_product", nameOfProduct);
            counter += 1;
        }

        if(!color.isEmpty()){
            if (counter != 0){
                SQL_SELECT_BY_ID.append(" and ");
            }
            SQL_SELECT_BY_ID.append("color = :color");
            params.put("color", color);
            counter += 1;
        }

        if(!amount.isEmpty()){
            if (counter != 0){
                SQL_SELECT_BY_ID.append(" and ");
            }
            SQL_SELECT_BY_ID.append("amount = :amount");
            params.put("amount", Integer.parseInt(amount));
            counter += 1;
        }

        if(!price.isEmpty()){
            if (counter != 0){
                SQL_SELECT_BY_ID.append(" and ");
            }
            SQL_SELECT_BY_ID.append("price= :price");
            params.put("price", Integer.parseInt(price));
            counter += 1;
        }

        if(!producer.isEmpty()){
            if (counter != 0){
                SQL_SELECT_BY_ID.append(" and ");
            }
            SQL_SELECT_BY_ID.append("producer= :producer");
            params.put("producer", producer);
        }

        SQL_SELECT_BY_ID.append(";");

        return namedParameterJdbcTemplate.query(SQL_SELECT_BY_ID.toString(), params, productMapper);

    }

    @Override
    public void save(Product product) {
        Map<String, Object> params = new HashMap<>();
        params.put("name_of_product", product.getNameOfProduct());
        params.put("color", product.getColor());
        params.put("amount", product.getAmount());
        params.put("price", product.getPrice());
        params.put("producer", product.getProducer());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("product")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(params)).longValue();

        product.setId(id);
    }

    @Override
    public List<Product> findAllByNameOrColorOrProducerLike(String query) {
        return namedParameterJdbcTemplate.query(SQL_LIKE_BY_NAME_OR_COLOR_OR_PRODUCER,
                Collections.singletonMap("query", "%" + query + "%"), productMapper);
    }

    @Override
    public List<Product> findAllProductSortedBy(String column, String descOrAsc) {

        String SQL_SELECT = SQL_SELECT_SORTED_BY + column + " " + descOrAsc;

        return namedParameterJdbcTemplate.query(SQL_SELECT, productMapper);
    }


}

