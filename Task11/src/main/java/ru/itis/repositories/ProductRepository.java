package ru.itis.repositories;

import ru.itis.models.Product;

import java.util.List;

public interface ProductRepository {

    List<Product> findAll();

    List <Product> findBy(String id, String nameOfProduct, String color, String amount, String price, String producer);

    void save(Product product);

    List<Product> findAllByNameOrColorOrProducerLike(String query);

    List<Product> findAllProductSortedBy(String column, String descOrAsc);

}
