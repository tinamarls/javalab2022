package ru.itis.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class AuthenticationManagerImpl implements AuthenticationManager{

    private final UserRepository usersRepository;

    @Override
    public User authenticate(String email, String password) {
        User user = usersRepository.findOneByEmail(email).orElse(null);

        if(user != null && user.getPassword().equals(password)){
            return user;
        } else {
            return null;
        }

    }

}
