package ru.itis.security.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Cookie;

import static ru.itis.constants.AllPaths.*;

import java.io.IOException;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_IN_PAGE) ||
                request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PAGE) ||
                request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PATH)) {
            filterChain.doFilter(request, response);
            return;
        }

        HttpSession session = request.getSession(false);

        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");
            if (authenticated != null && authenticated) {
                filterChain.doFilter(request, response);
                return;
            }
        }

        Cookie wantedPage = new Cookie("wantedPage", request.getRequestURI());
        wantedPage.setPath("/");
        response.addCookie(wantedPage);

        response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PAGE);


    }
}
