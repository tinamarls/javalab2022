package ru.itis.security.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import ru.itis.models.User;
import ru.itis.security.AuthenticationManager;

import static ru.itis.constants.AllPaths.*;

import java.io.IOException;
import java.util.Optional;

@WebFilter(value = {SIGN_IN_PATH, SIGN_IN_PAGE, SIGN_UP_PATH, SIGN_UP_PAGE})
public class SignInFilter implements Filter {

    private AuthenticationManager authenticationManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("applicationContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");
            if (authenticated != null && authenticated) {
                response.sendRedirect(APPLICATION_PREFIX + PROFILE_PATH);
                return;
            }
        }

        if (request.getMethod().equals("POST") &&
                (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_IN_PAGE) ||
                        request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_IN_PATH))) {
            String email = request.getParameter("username");
            String password = request.getParameter("password");

            User user = authenticationManager.authenticate(email, password);
            if(user != null){
                HttpSession sessionCheck = request.getSession(true);
                sessionCheck.setAttribute("authenticated", true);
                sessionCheck.setAttribute("currentUser", user);

                Cookie[] cookies = request.getCookies();

                if (cookies != null) {
                    for (Cookie cookie : cookies) {
                        if (cookie.getName().equals("wantedPage")) {
                            response.sendRedirect(cookie.getValue());
                            return;
                        }
                    }
                }

                response.sendRedirect(APPLICATION_PREFIX + PROFILE_PATH);
            } else {
                response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PAGE + "?error");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }



}
