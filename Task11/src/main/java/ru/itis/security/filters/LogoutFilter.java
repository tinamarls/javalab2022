package ru.itis.security.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import static ru.itis.constants.AllPaths.*;

import java.io.IOException;

@WebFilter("/logout")
public class LogoutFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        if(session != null){
            session.setAttribute("authenticated", false);
            response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PAGE);
            return;
        }
        filterChain.doFilter(request, response);

    }
}
