package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.user.HttpFormsConverter;
import ru.itis.dto.user.SignUpDto;
import ru.itis.services.UserService;

import static ru.itis.constants.AllPaths.*;

import java.io.IOException;

@WebServlet("/signUp")
public class UserServlet extends HttpServlet {

    private UserService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
        this.usersService = context.getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/html/signUp.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURI().equals(request.getContextPath() + "/signUp")) {
            SignUpDto signUpData = HttpFormsConverter.from(request);

            usersService.signUp(signUpData);
            response.sendRedirect(APPLICATION_PREFIX + PROFILE_PATH);
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
