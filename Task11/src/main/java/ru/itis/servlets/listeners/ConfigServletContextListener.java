package ru.itis.servlets.listeners;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.config.ApplicationConfig;

@WebListener
public class ConfigServletContextListener implements ServletContextListener {

    private HikariDataSource hikariDataSource;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        sce.getServletContext().setAttribute("applicationContext", context);

        this.hikariDataSource = context.getBean(HikariDataSource.class);

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        this.hikariDataSource.close();
    }
}
