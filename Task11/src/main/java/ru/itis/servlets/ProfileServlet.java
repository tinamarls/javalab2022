package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import static ru.itis.constants.AllPaths.*;

@WebServlet(PROFILE_PATH)
public class ProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("/profile.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(req.getParameter("columName") != null){
            String columnName = req.getParameter("columName");
            Cookie cookieForColumnName = new Cookie("columnName", columnName);
            cookieForColumnName.setPath("/");
            resp.addCookie(cookieForColumnName);
        }

        if(req.getParameter("desc") != null){
            String desc = req.getParameter("desc");
            Cookie cookieForTrueOrFalseDesc = new Cookie("desc", desc);
            cookieForTrueOrFalseDesc.setPath("/");
            resp.addCookie(cookieForTrueOrFalseDesc);
        }

        resp.sendRedirect(APPLICATION_PREFIX + PRODUCTS_PATH);

    }
}
