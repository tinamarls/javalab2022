package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.constants.AllPaths;
import ru.itis.dto.product.ProductDto;
import ru.itis.services.SearchService;

import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = AllPaths.LIVE_SEARCH_PATH)
public class LiveSearchServlet extends HttpServlet {

    private SearchService searchService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
        this.searchService = context.getBean(SearchService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query = req.getParameter("query");
        List<ProductDto> products = searchService.searchBy(query);
        String json = objectMapper.writeValueAsString(products);
        resp.setContentType("application/json");
        resp.getWriter().write(json);

    }
}
