package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static ru.itis.constants.AllPaths.*;

@WebServlet(value = {PRODUCTS_PATH, FIND_PATH})
public class ProductsServlet extends HttpServlet {

    private ProductService productService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
        this.productService = context.getBean(ProductService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getRequestURI().equals(APPLICATION_PREFIX + FIND_PATH)){
            req.getRequestDispatcher("/html/find.html").forward(req, resp);
        } else {
            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();

            Cookie[] cookies = req.getCookies();

            String columnName = null;
            String descTrueOrFalse = null;

            if(cookies != null){
                for(Cookie cookie: cookies){
                    if(cookie.getName().equals("columnName")){
                        columnName = cookie.getValue();
                    }
                    if(cookie.getName().equals("desc")){
                        descTrueOrFalse = cookie.getValue();
                    }
                }
            }

            List<Product> products = new ArrayList<>();

            if(columnName != null && descTrueOrFalse != null){
                products = productService.getAllOrderBy(columnName, descTrueOrFalse);
            } else {
                products = productService.getAllProducts();
            }
            
            String html = getHtmlForProducts(products);

            writer.println(html);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(req.getRequestURI().equals(APPLICATION_PREFIX + FIND_PATH)){
                String id = req.getParameter("id");
                String name = req.getParameter("nameForSearch");
                String color = req.getParameter("colorForSearch");
                String amount = req.getParameter("amountForSearch");
                String price = req.getParameter("priceForSearch");
                String producer = req.getParameter("producerForSearch");

                if(someNotEmpty(id, name, color, amount, price, producer)){

                    resp.setContentType("text/html");
                    PrintWriter writer = resp.getWriter();

                    List<Product> products = productService.getAllBy(id, name, color, amount, price, producer);
                    String html = getHtmlForProducts(products);

                    writer.println(html);
                }
        }

        if(req.getRequestURI().equals(APPLICATION_PREFIX + PRODUCTS_PATH)){

            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();

            List<Product> products = productService.getAllProducts();
            String html = getHtmlForProducts(products);

            writer.println(html);

        }

    }

    private static boolean paramsNotNull(String name, String color, String amount, String price, String producer) {
        return name != null && !name.equals("") && color != null && !color.equals("")
                && amount != null && !amount.equals("") && price != null && !price.equals("")
                && producer != null && !producer.equals("");
    }

    private static boolean someNotEmpty(String id, String name, String color, String amount, String price, String producer){
        return !id.equals("") || !name.equals("")
                || !color.equals("") || !amount.equals("")
                || !price.equals("") || !producer.equals("");
    }

    private static String getHtmlForProducts(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Products</title>\n" +
                "\t<link rel=\"stylesheet\" href=\"/app/css/style.css\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Список наших товаров</h1>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>Название товара</th>\n" +
                "\t\t<th>Цвет</th>\n" +
                "\t\t<th>Количество</th>\n" +
                "\t\t<th>Цена</th>\n" +
                "\t\t<th>Производитель</th>\n" +
                "\t</tr>");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getNameOfProduct()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("<td>").append(product.getAmount()).append("</td>\n");
            html.append("<td>").append(product.getPrice()).append("</td>\n");
            html.append("<td>").append(product.getProducer()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();

    }
}
