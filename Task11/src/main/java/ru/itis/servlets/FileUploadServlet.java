package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.models.FileInfo;
import ru.itis.models.User;
import ru.itis.services.FilesService;
import ru.itis.services.UserService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.itis.constants.AllPaths.*;

@WebServlet(UPLOAD_FILE_PATH)
@MultipartConfig
public class FileUploadServlet extends HttpServlet {

    private FilesService filesService;
    private UserService userService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
        this.filesService = context.getBean(FilesService.class);
        this.userService = context.getBean(UserService.class);
    }

//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.getRequestDispatcher("/html/upload.html").forward(req, resp);
//    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String description = (new BufferedReader(
                new InputStreamReader(request.getPart("description").getInputStream())).readLine());

        Part filePart = request.getPart("file");

        FileDto uploadedFileInfo = FileDto.builder()
                .size(filePart.getSize())
                .description(description)
                .mimeType(filePart.getContentType())
                .fileName(filePart.getSubmittedFileName())
                .fileStream(filePart.getInputStream())
                .build();

        FileInfo fileInfo = filesService.upload(uploadedFileInfo);

        User user = (User) request.getSession().getAttribute("currentUser");
        userService.addPhotoForUser(user, fileInfo);

        response.sendRedirect(APPLICATION_PREFIX + PROFILE_PATH);
    }
}

