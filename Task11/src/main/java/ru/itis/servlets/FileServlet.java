package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.models.User;
import ru.itis.services.FilesService;

import java.io.IOException;
import java.nio.file.Files;

import static ru.itis.constants.AllPaths.*;

@WebServlet(FILES_PATH)
public class FileServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
        this.filesService = context.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("currentUser");
        System.out.println("file for user " + user);
        FileDto file = filesService.getFile(user.getFileId());


        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        response.setHeader("Content-Disposition", "fileName=\"" + file.getOriginalFileName() + "\"");
        Files.copy(file.getPath(), response.getOutputStream());
        response.flushBuffer();
    }

}
