package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.product.ProductDto;
import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.io.IOException;
import java.util.List;

import static ru.itis.constants.AllPaths.*;

@WebServlet(SAVE_PATH)
public class SaveProductServlet extends HttpServlet {

    private ProductService productService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
        this.productService = context.getBean(ProductService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("/html/add.html").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String body = req.getReader().readLine();
        ProductDto productDto = objectMapper.readValue(body, ProductDto.class);

        productService.insertProduct(productDto);

        List<Product> products = productService.getAllProducts();
        List<ProductDto> productDtoList = ProductDto.fromList(products);

        String jsonResponse = objectMapper.writeValueAsString(productDtoList);
        resp.setContentType("application/json");
        resp.getWriter().write(jsonResponse);


    }

}

