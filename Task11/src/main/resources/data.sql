insert into product(name_of_product, color, amount, price, producer)
values ('Киндер пингви', 'цветной', 2000, 60, 'Kinder');

insert into product(name_of_product, color, amount, price, producer)
values ('Носки', 'белый',10000, 65, 'Твое');

insert into product(name_of_product, color, amount, price, producer)
values ('Носки', 'цветной', 8000 , 85, 'Твое');

insert into product(name_of_product, color, amount, price, producer)
values ('Плед', 'серый', 2500, 1500, 'IKEA');

insert into product(name_of_product, color, amount, price, producer)
values ('Солнцезащитные очки', 'черный', 150, 32000, 'Dior');

insert into product(name_of_product, color, amount, price, producer)
values ('Наушники беспроводные', 'белый', 8000, 30000, 'Xiaomi');

insert into product(name_of_product, color, amount, price, producer)
values ('Наушники беспроводные', 'черный', 7500, 29599, 'JBL');
