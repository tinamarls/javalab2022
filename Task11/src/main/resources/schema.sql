drop table if exists product;
drop table if exists student;

create table product(
    id bigserial primary key,
    name_of_product varchar(50),
    color varchar(20),
    amount integer,
    price integer,
    producer varchar(30)
);

create table file_info (
   id bigserial primary key,
   original_file_name varchar(1000),
   storage_file_name varchar(100),
   size bigint,
   mime_type varchar(50),
   description varchar(1000)
);

create table users(
    id bigserial primary key,
    first_name varchar(30),
    last_name varchar(30),
    age int,
    email varchar(30),
    password varchar(30),
    id_file_info bigint references file_info(id)
);

update users set id_file_info = 7 where id = 1;



