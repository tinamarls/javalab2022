function searchProduct(query){
    return fetch('/app/search/live?query=' + query)
        .then((response) => {
            return response.json()
        }).then((products) => {
            fillTable(products)
        })
}

function fillTable(products){

    let table = document.getElementById("productsTable");

    table.innerHTML = '    <tr>\n' +
        '        <th>id</th>\n' +
        '        <th>Название продукта</th>\n' +
        '        <th>Цвет</th>\n' +
        '        <th>Количество на складе</th>\n' +
        '        <th>Производитель</th>\n' +
        '        <th>Цена</th>\n' +
        '    </tr>';

    for(let i = 0; i < products.length; i++){
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let nameCell = row.insertCell(1);
        let colorCell = row.insertCell(2);
        let amountCell = row.insertCell(3);
        let producerCell = row.insertCell(4);
        let priceCell = row.insertCell(5);

        idCell.innerHTML = products[i].id;
        nameCell.innerHTML = products[i].name;
        colorCell.innerHTML = products[i].color;
        amountCell.innerHTML = products[i].amount;
        producerCell.innerHTML = products[i].producer;
        priceCell.innerHTML = products[i].price;

    }
}

function addProduct(name, color, amount, price, producer){
    let body = {
        "name" : name,
        "color" : color,
        "amount" : amount,
        "price" : price,
        "producer" : producer
    }

    fetch('/app/save', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => response.json())
        .then((products) => fillTable(products))
        .catch((error) => {
            alert((error))
        })
}