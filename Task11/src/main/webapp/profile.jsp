<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Профиль</title>
    <link rel="stylesheet" href="/app/css/style.css">
</head>

<body>
    <form action="/app/index.html">
        <button type="submit">Вернуться в меню</button>
    </form>

    <h1>Ваш профиль</h1>
    <p>Ваша фоточка:</p

    <c:set var="fileId" value="${sessionScope.currentUser.fileId}" />
    <div>
        <img src="/app/files?fileName=${fileId}" alt="нет фото" width="200" height="200">
    </div>


    <h4>Для загрузки аватарки:</h4>
    <form action="/app/files/upload" method="post" enctype="multipart/form-data">
        <label>
            Описание
            <input type="text" name="description" placeholder="Введи описание..">
        </label>
        <input type="file" name="file">
        <input type="submit" value="Загрузить">
    </form>

    <h3>Если установить параметры для сортировки всех таблиц на сайте</h3>

    <form action="/app/profile" method="POST">
        <label for="select">По какому столбцу</label>
        <select name="columName" id="select">
            <option value="id">id</option>
            <option value="name_of_product">Название</option>
            <option value="color">Цвет</option>
            <option value="amount">Количество на складе</option>
            <option value="producer">Изготовитель</option>
            <option value="price">Цена</option>
        </select>

        <label for="select2">По возрастанию или убыванию</label>
        <select name="desc" id="select2">
            <option value="ASC">По возрастанию</option>
            <option value="DESC">По убыванию</option>
        </select>

        <button type="submit" class="buttonClick">Фильтрация пользователей</button>
    </form>

</body>
</html>