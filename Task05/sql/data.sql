insert into student(first_name, last_name, age)
values ('Марсель', 'Сидиков', 28);
insert into student(first_name, last_name, age)
values ('Айрат', 'Мухутдинов', 23);
insert into student(first_name, last_name, age)
values ('Виктор', 'Евлампьев', 23);
insert into student(first_name, last_name, age)
values ('Максим', 'Поздеев', 21);

-- обновление данных в таблице
update student
set phone_number = '79377777777',
    age          = 24
where id = 2;

insert into course (title, description, start_date, finish_date)
values ('Java', 'Изучаем разработку на Java', '2020-02-02', '2021-02-02'),
       ('Python', 'Изучаем разработку на Python', '2020-03-03', '2021-03-03'),
       ('.NET', 'Изучаем разработку на .NET', '2020-04-04', '2021-04-04'),
       ('SQL', 'Изучаем SQL', '2020-05-05', '2021-05-05');

INSERT INTO lesson (name, start_time, finish_time, day_of_week, course_id)
VALUES ('Generics', '12:00:00', '13:00:00', 'MONDAY', 1);
INSERT INTO lesson (name, start_time, finish_time, day_of_week, course_id)
VALUES ('OOP', '13:00:00', '14:00:00', 'FRIDAY', 1);
INSERT INTO lesson (name, start_time, finish_time, day_of_week, course_id)
VALUES ('Arrays', '14:00:00', '15:00:00', 'SUNDAY', 2);
INSERT INTO lesson (name, start_time, finish_time, day_of_week, course_id)
VALUES ('OOP', '15:00:00', '16:00:00', 'FRIDAY', 2);
INSERT INTO lesson (name, start_time, finish_time, day_of_week, course_id)
VALUES ('ASP', '16:00:00', '17:00:00', 'SUNDAY', 3);
INSERT INTO lesson (name, start_time, finish_time, day_of_week, course_id)
VALUES ('OOP', '16:00:00', '17:00:00', 'MONDAY', 3);
INSERT INTO lesson (name, start_time, finish_time, day_of_week, course_id)
VALUES ('Tables', '17:00:00', '18:00:00', 'FRIDAY', 4);
INSERT INTO lesson (name, start_time, finish_time, day_of_week, course_id)
VALUES ('Queries', '18:00:00', '19:00:00', 'SUNDAY', 4);

INSERT INTO student_course (student_id, course_id)
VALUES (1, 1);
INSERT INTO student_course (student_id, course_id)
VALUES (1, 2);
INSERT INTO student_course (student_id, course_id)
VALUES (2, 2);
INSERT INTO student_course (student_id, course_id)
VALUES (2, 3);
INSERT INTO student_course (student_id, course_id)
VALUES (2, 4);
INSERT INTO student_course (student_id, course_id)
VALUES (3, 3);
INSERT INTO student_course (student_id, course_id)
VALUES (4, 1);