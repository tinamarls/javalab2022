package ru.itis;

import ru.itis.dto.StudentSignUp;
import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);
        StudentsService studentsService = new StudentsServiceImpl(studentsRepository);

        Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);

        studentsRepository.delete(20L);
        System.out.println(studentsRepository.findAll());

        studentsRepository.update(new Student(17L,"Костя", "Егоров", 69, "smetan@mail.ru", "blablabla"));
        System.out.println(studentsRepository.findAll());

        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(20));
    }
}
