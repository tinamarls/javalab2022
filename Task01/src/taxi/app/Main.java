package taxi.app;

import taxi.dto.SignUpForm;
import taxi.mappers.Mappers;
import taxi.models.User;
import taxi.repositories.UsersRepository;
import taxi.repositories.UsersRepositoryFilesImpl;
import taxi.services.UsersService;
import taxi.services.UsersServiceImpl;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        usersService.signUp(new SignUpForm("Кристина", "Коржуева",
                "kkorzhuyeva11@mail.ru", "blabla223"));
        usersService.signUp(new SignUpForm("Петров", "Петя",
                "petrovpetya@mail.ru", "kgvjnbrk23"));
        usersService.signUp(new SignUpForm("Иванов", "Иван",
                "ivanov@mail.ru", "l34ijbc"));
        usersService.signUp(new SignUpForm("Семенов", "Ваня",
                "semvanya@mail.ru", "kpdlcm35hj"));

        List<User> listOfUsers = usersRepository.findAll();

        User userForDelete = new User("Семенов", "Ваня",
                "semvanya@mail.ru", "kpdlcm35hj");
        User userForUpdate = new User(listOfUsers.get(1).getId(), "Кристина", "Коржуева",
                "tinamarls@mail.ru", "plo98cmkv");

        usersRepository.delete(userForDelete);
        usersRepository.update(userForUpdate);
        usersRepository.deleteById(listOfUsers.get(3).getId());
        System.out.println(usersRepository.findById(listOfUsers.get(0).getId()));

    }
}
