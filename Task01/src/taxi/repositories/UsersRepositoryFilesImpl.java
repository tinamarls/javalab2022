package taxi.repositories;

import taxi.models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;


public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    private static final Function<String[], User> stringToUser = dataOfUser ->
            new User(UUID.fromString(dataOfUser[0]), dataOfUser[1], dataOfUser[2], dataOfUser[3], dataOfUser[4]);

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> listOfUsers = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] dataOfUser = line.split("\\|");
                listOfUsers.add(stringToUser.apply(dataOfUser));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return listOfUsers;

    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        File temp = new File("tempFile.txt");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
             Writer writer = new FileWriter(temp, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] dataOfUser = line.split("\\|");
                String idOfUserForUpdate = entity.getId().toString();
                if (idOfUserForUpdate.equals(dataOfUser[0])) {
                    String userAsString = userToString.apply(entity);
                    bufferedWriter.write(userAsString);
                } else {
                    bufferedWriter.write(line);
                }
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        File originalFile = new File(fileName);
        originalFile.delete();
        temp.renameTo(originalFile);
    }

    @Override
    public void delete(User entity) {
        File temp = new File("tempFile.txt");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
             Writer writer = new FileWriter(temp, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] dataOfUser = line.split("\\|");
                if (!dataOfUser[1].equals(entity.getFirstName()) || !dataOfUser[2].equals(entity.getLastName())
                        || !dataOfUser[3].equals(entity.getEmail()) || !dataOfUser[4].equals(entity.getPassword())) {
                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        File originalFile = new File(fileName);
        originalFile.delete();
        temp.renameTo(originalFile);
    }

    @Override
    public void deleteById(UUID id) {
        File temp = new File("tempFile.txt");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
             Writer writer = new FileWriter(temp, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] dataOfUser = line.split("\\|");
                String idOfUserForDelete = id.toString();
                if (!idOfUserForDelete.equals(dataOfUser[0])) {
                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        File originalFile = new File(fileName);
        originalFile.delete();
        temp.renameTo(originalFile);

    }

    @Override
    public User findById(UUID id) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] dataOfUser = line.split("\\|");
                String idOfUser = id.toString();
                if (idOfUser.equals(dataOfUser[0])) {
                    return stringToUser.apply(dataOfUser);
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }

}
