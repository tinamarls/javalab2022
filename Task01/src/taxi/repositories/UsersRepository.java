package taxi.repositories;


import taxi.models.User;

import java.util.UUID;

public interface UsersRepository extends CrudRepository<UUID, User> {
}
