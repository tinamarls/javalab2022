package taxi.services;

import taxi.dto.SignUpForm;

public interface UsersService {
    void signUp(SignUpForm signUpForm);
}
