--1
select model, speed, hd
from PC
where price < 500;

--2
select distinct maker
from Product
where type = 'Printer';

--3
select model, ram, screen
from Laptop
where price > 1000;

--4
select *
from Printer
where color = 'y';

--5
select model, speed, hd
from PC
where price < 600
and (cd = '12x' or cd = '24x');

--6
select distinct p.maker, l.speed
from Product p
         inner join Laptop l on p.model = l.model
where l.hd >= 10;

--7
select PC.model, price
from PC
         inner join Product on Product.model = PC.model
where Product.maker = 'B'
union
select Laptop.model, price
from Laptop
         inner join Product on Product.model = Laptop.model
where Product.maker = 'B'
union
select Printer.model, price
from Printer
         inner join Product on Product.model = Printer.model
where Product.maker = 'B';

--8
select maker
from Product
where type = 'PC'
except
select maker
from Product
where type = 'Laptop';

--9
select distinct maker
from Product p
         inner join PC pc on p.model = pc.model
where pc.speed >= 450;

--10
select model, price
from Printer p
where p.price = (select MAX(price) from Printer);

--11
select AVG(speed)
from PC;

--12
select AVG(speed)
from Laptop
where price > 1000;

--13
select AVG(pc.speed)
from PC pc
         inner join Product p on p.model = pc.model
where maker = 'A';

--14
select s.class, s.name, cl.country
from Ships s
         inner join Classes cl on s.class = cl.class
where numGuns >= 10;

--15
select hd
from PC
group by hd
having count(hd) >= 2;

--16
select distinct p.model, k.model, p.speed, p.ram
from PC k,
     PC p
where k.speed = p.speed
  and k.ram = p.ram
  and k.model < p.model;

--17
select distinct p.type, p.model, l.speed
from Product p
         inner join Laptop l on p.model = l.model
where speed < all (select speed from PC);

--18
select distinct p.maker, pr.price
from Product p
         inner join Printer pr on p.model = pr.model
where color = 'y'
  and pr.price = (select MIN(price) from Printer where color = 'y');

--19
select p.maker, AVG(screen)
from Product p
         inner join Laptop l on l.model = p.model
group by p.maker;

--20
select maker, count(model)
from Product
where type = 'PC'
group by (maker)
having count(model) >= 3;

--21
select p.maker, MAX(pc.price)
from Product p
         inner join PC pc on pc.model = p.model
group by (p.maker);

--22
select speed, AVG(price)
from PC
where speed > 600
group by speed;

--23
select p.maker
from Product p
         inner join PC pc on pc.model = p.model
where pc.speed >= 750
intersect
select p.maker
from Product p
         inner join Laptop l on l.model = p.model
where l.speed >= 750;

--24
with all_tables as (select model, price
                    from PC
                    union all
                    select model, price
                    from Laptop
                    union all
                    select model, price
                    from Printer)

select distinct model
from all_tables
where price = (select MAX(price) from all_tables);

--25
select distinct p.maker
from Product p
         inner join PC pc on p.model = pc.model
where pc.ram = (select MIN(ram)
                from PC)
and pc.speed = (select MAX(speed)
                from PC
                where ram in (select MIN(RAM) from PC))
and p.maker in (select maker
                from Product
                where type = 'Printer');

--26
with two_tables as (select pc.price
                    from Product p
                             inner join PC pc on pc.model = p.model
                    where maker = 'A'
                    union all
                    select l.price
                    from Product p
                             inner join Laptop l on l.model = p.model
                    where maker = 'A')

select AVG(price)
from two_tables;

--27
select p.maker, AVG(hd)
from Product p
         right join PC pc on pc.model = p.model
where p.maker in (select maker
                  from Product
                  where type = 'Printer')
group by p.maker;

--28
select distinct count(maker)
from Product
where maker in (select maker
                from Product
                group by maker
                having count(model) = 1);

--31
select distinct cl.class, cl.country
from Classes cl
where cl.bore >= 16;

--33
select ship
from Outcomes
where battle = 'North Atlantic'
  and result = 'sunk';

--34
select distinct name
from Classes cl,
     Ships s
where launched >= 1922
  and displacement > 35000
  and type = 'bb'
  and s.class = cl.class;

--35
select model, type
from Product
where model not like '%[^0-9]%'
   or model not like '%[^A-Z]%';

--36
select class
from Classes
where class in (select name from Ships)
union
select class
from Classes
where class in (select ship from Outcomes);

--37
with two_tables as (select class, name
                    from Ships
                    union
                    select ship, ship
                    from Outcomes out)

select s.class
from Classes s
         inner join two_tables t on t.class = s.class
group by s.class
having count(t.name) = 1;

--38
select country
from Classes
where type = 'bb'
intersect
select country
from Classes
where type = 'bc';

--39
select distinct ship
from Outcomes out
         inner join Battles bat on bat.name = out.battle
where out.result = 'damaged'
  and exists(
        select ship, date
        from Outcomes o inner join Battles b on b.name = o.battle
        where o.ship = out.ship and b.date > bat.date
    );

--40
select distinct maker, type
from Product product
where maker in (select maker
                from Product
                group by maker
                having count(model) > 1)
  and not exists(
        select type
        from Product p
        where product.maker = p.maker
          and product.type <> p.type);

--42
select ship, battle
from Outcomes
where result = 'sunk';

--43
select name
from Battles
where year (date) not in (
    select launched
    from Ships where launched is not null
    );

--44
select name
from Ships
where name like 'R%'
union
select ship
from Outcomes
where ship like 'R%';

--45
select name
from Ships
where name like '% % %'
union
select ship
from Outcomes
where ship like '% % %';

