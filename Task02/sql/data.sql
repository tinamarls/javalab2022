insert into client (first_name, last_name, phone_number)
values ('Марсель', 'Сидиков', '89877777777'),
       ('Кристина', 'Коржуева', '89870379906'),
       ('Арина', 'Мурсалимова', '89178768903'),
       ('Костя', 'Егоров', '89238756678');

insert into car (mark, color, year_of_manufacture, number)
values ('Hyundai', 'серый', 2015, 'Е123КА102'),
       ('Toyota', 'белый', 2011, 'М546ОТ77'),
       ('BMW', 'красный', 2017, 'К111КК102'),
       ('Mercedes', 'черный', 2010, 'Т754МН116'),
       ('KIA', 'синий', 2012, 'T234НМ');

insert into driver (first_name, last_name, phone_number, work_experience, car_id)
values ('Александр', 'Бурчак', '89178016398', 5, 4),
       ('Алексей', 'Бахчев', '89178016586', 1, 1),
       ('Павел', 'Павлов', '89377658990', 10, 5),
       ('Тимур', 'Абубакирова', '89652134567', 7, 3);

insert into ordering (price, departure_address, arrival_address, driver_id, client_id)
values (150, 'ДУ', 'Двойка', 4, 2),
       (2000, 'Салават', 'Казань', 3, 3),
       (1000, 'Казань', 'Чебоксары', 1, 4),
       (4000, 'Анталья', 'Стамбул', 2, 1);

update ordering
set arrival_address = 'Сеул'
where client_id = 4;

update ordering
set departure_address = 'Казань',
    arrival_address   = 'Санкт-Петербург'
where client_id = 2;




