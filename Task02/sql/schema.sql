drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists ordering;

create table client
(
    id           bigserial primary key,
    first_name   char(20),
    last_name    char(20),
    phone_number char(11)
);

create table driver
(
    id              serial primary key,
    first_name      char(20),
    last_name       char(20),
    phone_number    char(11),
    work_experience int
);

create table car
(
    id                  serial primary key,
    mark                char(20),
    color               char(20),
    year_of_manufacture int,
    number              char(10)
);

create table ordering
(
    id                serial primary key,
    price             int,
    departure_address char(200),
    arrival_address   char(200),
    driver_id         bigint,
    client_id         bigint
);

alter table driver
    add car_id bigint;

alter table driver
    add foreign key (car_id) references car (id);

alter table ordering
    add foreign key (driver_id) references driver (id);

alter table ordering
    add foreign key (client_id) references client (id);



