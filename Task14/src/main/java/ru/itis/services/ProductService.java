package ru.itis.services;

import ru.itis.dto.ProductDto;
import ru.itis.models.Product;

import java.util.List;

public interface ProductService {

    void insertProduct(ProductDto productDto);

    List<Product> getAllProducts();

    List<Product> getAllBy(String id, String nameOfProduct, String color, String amount, String price, String producer);

    List<Product> getAllOrderBy(String columnName, String descOrAsc);
}
