package ru.itis.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.util.List;

import static ru.itis.constants.AllPaths.PRODUCTS_PATH;


@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
@Controller
@RequestMapping(value = PRODUCTS_PATH)
public class ProductsController {

    ProductService productService;

    @RequestMapping
    public String getProductsPage(Model model, @CookieValue("columnName") String column,
                                    @CookieValue("desc") String desc) {
        List<Product> products = productService.getAllOrderBy(column, desc);
        model.addAttribute("products", products);
        return "products";
    }
}
