package ru.itis.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static ru.itis.constants.AllPaths.*;

@Controller
public class MenuController {
    @RequestMapping(EMPTY_PATH)
    public String getMenuPage() {
        return "index";
    }

}
