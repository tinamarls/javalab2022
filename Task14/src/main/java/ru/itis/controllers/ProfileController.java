package ru.itis.controllers;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

import static ru.itis.constants.AllPaths.*;

@RequiredArgsConstructor
@Controller
@RequestMapping(value = PROFILE_PATH)
public class ProfileController {
    @RequestMapping
    public String getProfilePage() {
        return "profile";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void setSortOfTables(@RequestParam("columName") String columName,
                                  @RequestParam("desc") String desc,
                                  HttpServletResponse resp) throws IOException {

        if(columName != null){
            Cookie cookieForColumnName = new Cookie("columnName", columName);
            cookieForColumnName.setPath("/");
            resp.addCookie(cookieForColumnName);
        }
        if(desc != null){
            Cookie cookieForTrueOrFalseDesc = new Cookie("desc", desc);
            cookieForTrueOrFalseDesc.setPath("/");
            resp.addCookie(cookieForTrueOrFalseDesc);
        }

        resp.sendRedirect(APPLICATION_PREFIX + PRODUCTS_PATH);

    }


}
