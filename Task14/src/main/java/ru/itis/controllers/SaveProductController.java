package ru.itis.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.ProductDto;
import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.util.List;

import static ru.itis.constants.AllPaths.*;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
@Controller
@RequestMapping(value = SAVE_PATH)
public class SaveProductController {

    ProductService productService;

    @RequestMapping
    public String getSavePage(Model model) {
        List<Product> products = productService.getAllProducts();
        List<ProductDto> productDtoList = ProductDto.fromList(products);
        model.addAttribute("products", productDtoList);
        return "add";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public List<ProductDto> addUser(@RequestBody ProductDto productDto) {

        productService.insertProduct(productDto);

        List<Product> products = productService.getAllProducts();
        return ProductDto.fromList(products);

    }

}
