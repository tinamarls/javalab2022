package ru.itis.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.itis.dto.ProductDto;
import ru.itis.services.SearchService;

import java.util.List;

import static ru.itis.constants.AllPaths.*;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
@Controller
@RequestMapping(value = SEARCH_PATH)
public class LiveSearchController {
    SearchService searchService;

    @RequestMapping
    public String getLiveSearchPage() {
        return "search";
    }

    @RequestMapping(value = LIVE_SEARCH_PATH)
    @ResponseBody
    public List<ProductDto> searchUsers(@RequestParam("query") String query) {
        return searchService.searchBy(query);
    }

}
