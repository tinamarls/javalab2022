package ru.itis.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.util.List;

import static ru.itis.constants.AllPaths.FIND_PATH;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
@Controller
@RequestMapping(value = FIND_PATH)
public class FindController {

    ProductService productService;

    @RequestMapping
    public String getFindPage() {
        return "find";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String findAllProductsBy(Model model, @RequestParam("id") String id,
                                    @RequestParam("nameForSearch") String nameForSearch,
                                    @RequestParam("colorForSearch") String colorForSearch,
                                    @RequestParam("amountForSearch") String amountForSearch,
                                    @RequestParam("priceForSearch") String priceForSearch,
                                    @RequestParam("producerForSearch") String producerForSearch) {

        List<Product> products = productService.getAllBy(id, nameForSearch, colorForSearch, amountForSearch, priceForSearch, producerForSearch);

        model.addAttribute("products", products);

        return "products";

    }

}
