package ru.itis.constants;

public class AllPaths {

    public static final String APPLICATION_PREFIX = "/app";

    public static final String EMPTY_PATH = "/";

    public static final String SEARCH_PATH = "/search";

    public static final String LIVE_SEARCH_PATH = "/live";

    public static final String PRODUCTS_PATH = "/products";

    public static final String SAVE_PATH = "/save";

    public static final String FIND_PATH = "/find";

    public static final String PROFILE_PATH = "/profile";


}
