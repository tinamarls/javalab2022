package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.models.Product;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ProductDto {

    private Long id;
    private String name;
    private String color;
    private int amount;
    private String producer;
    private int price;

    public static ProductDto from(Product product){
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getNameOfProduct())
                .color(product.getColor())
                .amount(product.getAmount())
                .producer(product.getProducer())
                .price(product.getPrice())
                .build();
    }

    public static List<ProductDto> fromList(List<Product> products){
        return products.stream().map(ProductDto::from).collect(Collectors.toList());
    }

}
