public class Main {
    public static void main(String[] args) {
        SqlGenerator sqlGenerator = new SqlGenerator();
        String create = sqlGenerator.createTable(User.class);
        System.out.println(create);

        User user = new User("Кристина", "Коржуева", true);
        User user2 = new User("Паша");

        String insert = sqlGenerator.insert(user);
        System.out.println(insert);
        System.out.println(sqlGenerator.insert(user2));
    }
}
