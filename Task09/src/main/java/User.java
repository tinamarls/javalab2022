
@TableName("account")

public class User {

    @ColumnName(name = "id", primary = true, identity = true)
    private Long id;

    @ColumnName(name = "first_name", maxLength = 25)
    private String firstName;

    @ColumnName(name = "last_name")
    private String lastName;

    @ColumnName(name = "id_worker", defaultBoolean = false)
    private boolean isWorker;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User(String firstName) {
        this.firstName = firstName;
    }

    public User(String firstName, String lastName, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
    }

}
