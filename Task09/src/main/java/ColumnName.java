import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ColumnName {

    String name();

    int maxLength() default 65001;

    boolean primary() default false;

    boolean identity() default false;

    boolean defaultBoolean() default false;


}
