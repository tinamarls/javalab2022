import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SqlGenerator {

    public <T> String createTable(Class<T> entityClass){

        StringBuilder code = new StringBuilder();

        TableName tableName = entityClass.getAnnotation(TableName.class);
        String name = tableName.value();
        code.append("create table ").append(name).append(" (").append("\n");

        Field[] fields =entityClass.getDeclaredFields();

        int i = fields.length - 1;
        for(Field field: fields){
            ColumnName columnName = field.getAnnotation(ColumnName.class);
            code.append("\t").append(columnName.name());

            if (field.getType().equals(String.class)){
                code.append(" varchar");
                if (columnName.maxLength()!=65001){
                    code.append("(").append(columnName.maxLength()).append(")");
                }
            }

            if (field.getType().equals(Integer.TYPE)){
                code.append(" int");
            }

            if (field.getType().equals(Double.TYPE)){
                code.append(" double precision");
            }

            if (field.getType().equals(Boolean.TYPE)){
                code.append(" boolean ");
                if (columnName.defaultBoolean()){
                    code.append("default true");
                } else{
                    code.append("default false");
                }
            }

            if(field.getType().equals(Long.class)){
                code.append(" bigint");
                if (columnName.primary()){
                    code.append(" primary key");
                }
                if (columnName.identity()){
                    code.append(" generated always as identity");
                }

            }
            if (i-- != 0){
                code.append(",").append("\n");
            }
        }

        code.append("\n").append(");");
        return code.toString();

    }

    public <T> String insert(Object entity){
        Class<T> aClass = (Class<T>) entity.getClass();


        StringBuilder insertCode = new StringBuilder();

        TableName tableName = aClass.getAnnotation(TableName.class);
        String name = tableName.value();
        insertCode.append("insert into ").append(name).append(" (");

        Field[] fields = aClass.getDeclaredFields();

        List<String> parameters = new ArrayList<>();
        int i = fields.length -2;
        for (Field field : fields){
            try {
                field.setAccessible(true);
                Object p = field.get(entity);
                ColumnName columnName = field.getAnnotation(ColumnName.class);
                if (!columnName.name().equals("id")){
                    if (p == null){
                        parameters.add("null");
                    } else if(field.getType().equals(String.class)){
                        String string = "'" + p + "'";
                        parameters.add(string);
                    } else{
                        parameters.add(p.toString());
                    }

                    insertCode.append(columnName.name());
                    if(i-- != 0 ){
                        insertCode.append(", ");
                    }
                }

            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        insertCode.append(")").append("\n").append("values(");

        int size = parameters.size();
        for (int j = 0; j < size; j++) {
            insertCode.append(parameters.get(j));
            if (j != size -1){
                insertCode.append(", ");
            }
        }

        insertCode.append(");");

        return insertCode.toString();

    }
}
