package ru.itis;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


@Mojo(name = "list-of-size-classes", defaultPhase = LifecyclePhase.COMPILE)
public class ListOfSizeClassesMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFolderFileName;

    @Parameter(defaultValue = "${project.build.sourceDirectory}", required = true)
    private String sourceFolderFileName;

    @Parameter(name = "listOfSizeClassesFileName", required = true)
    private String listOfSizeClassesFileName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File outputFolder = new File(outputFolderFileName);

        File listOfSizeClassesFile = new File(outputFolder,listOfSizeClassesFileName);

        try(BufferedWriter writer = new BufferedWriter(new FileWriter(listOfSizeClassesFile))) {
            getLog().info("Output file for list of size classes is - " + listOfSizeClassesFileName);

            Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file ->{
                        try{
                            writer.write(file.getFileName().toString() + " : размер этого файла - " + Files.size(file));
                            writer.newLine();

                        } catch(IOException e){
                            throw new IllegalArgumentException(e);
                        }
                    });

            getLog().info("Finished work");

        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }
}
